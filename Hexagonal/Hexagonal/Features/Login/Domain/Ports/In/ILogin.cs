﻿using Hexagonal.Features.Login.Domain.Models;

namespace Hexagonal.Features.Login.Domain.Ports.In
{
    public interface ILogin
    {
        Task<string> Login(LoginModel login);
    }
}
