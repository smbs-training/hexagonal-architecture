﻿namespace Hexagonal.Features.Course.Domain.Ports.In
{
    public interface ICourseServicePort : IGetCourseById, IGetCourses, ICreateCourse, IUpdateCourse, IDeleteCourse
    {
    }
}
