﻿using Hexagonal.Features.Course.Domain.Models;

namespace Hexagonal.Features.Course.Domain.Ports.In
{
    public interface IUpdateCourse
    {
        Task<CourseModel> Update(int id, CourseModel course);
    }
}
