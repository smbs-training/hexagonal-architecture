﻿using Hexagonal.Features.Course.Domain.Models;
using Hexagonal.Features.Course.Domain.Ports.In;
using Hexagonal.Features.Course.Domain.Ports.Out;

namespace Hexagonal.Features.Course.Application.UseCases
{
    public class GetCoursesImpl : IGetCourses
    {
        private readonly ICourseDbAdapterPort _adapter;

        public GetCoursesImpl(ICourseDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<List<CourseModel>> GetCourses()
        {
            return _adapter.FindAll();
        }
    }
}
