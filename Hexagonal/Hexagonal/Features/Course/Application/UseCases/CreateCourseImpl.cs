﻿using Hexagonal.Features.Course.Domain.Models;
using Hexagonal.Features.Course.Domain.Ports.In;
using Hexagonal.Features.Course.Domain.Ports.Out;

namespace Hexagonal.Features.Course.Application.UseCases
{
    public class CreateCourseImpl: ICreateCourse
    {
        private readonly ICourseDbAdapterPort _adapter;

        public CreateCourseImpl(ICourseDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<CourseModel> Create(CourseModel course)
        {
            return _adapter.Create(course);
        }
    }
}
