﻿using Hexagonal.Features.Course.Domain.Models;
using Hexagonal.Features.User.Infrastructure;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hexagonal.Features.Course.Infrastructure
{
    [Table("Course")]
    public class CourseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; } = null!;

        [MaxLength(500)]
        public string? Description { get; set; }

        [ForeignKey("InstructorId")]
        public int InstructorId { get; set; }

        public virtual UserEntity Instructor { get; set; }

        public virtual ICollection<UserEntity> Students { get; set; }

        public CourseEntity FromModel(CourseModel model)
        {
            Id = model.Id;
            Name = model.Name;
            Description = model.Description;
            InstructorId = model.InstructorId;

            return this;
        }

        public CourseModel ToModel()
        {
            var model = new CourseModel
            {
                Id = Id,
                Name = Name,
                Description = Description,
                InstructorId = InstructorId
            };

            return model;
        }

        public void UpdatePropsFromModel(CourseModel model)
        {
            Name = model.Name ?? Name;
            Description = model.Description ?? Description;

            if (!model.InstructorId.Equals(default))
                InstructorId = model.InstructorId;
        }
    }
}
