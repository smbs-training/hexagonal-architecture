﻿using Hexagonal.Features.Course.Domain.Models;
using Hexagonal.Features.Course.Domain.Ports.In;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Hexagonal.Features.Course.Infrastructure
{
    [Route("api/Courses")]
    [ApiController]
    [Authorize]
    public class CourseController : ControllerBase
    {
        private readonly ICourseServicePort _service;

        public CourseController(ICourseServicePort service)
        {
            _service = service;
        }

        // GET: api/<CourseController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CourseModel>>> Get()
        {
            return await _service.GetCourses();
        }

        // GET api/<CourseController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CourseModel>> Get(int id)
        {
            CourseModel courseModel = await _service.GetCourseById(id);
            if (courseModel == null)
            {
                return NotFound();
            }
            return courseModel;
        }

        // POST api/<CourseController>
        [HttpPost]
        public async Task<ActionResult<CourseModel>> Post([FromBody] CourseModel courseModel)
        {
            try
            {
                CourseModel modelSaved = await _service.Create(courseModel);
                return CreatedAtAction("Get", new { id = modelSaved.Id }, modelSaved);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PATCH api/<CourseController>/5
        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(int id, [FromBody] CourseModel courseModel)
        {
            try
            {
                await _service.Update(id, courseModel);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return NoContent();
        }

        // DELETE api/<CourseController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _service.Delete(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return NoContent();
        }
    }
}
