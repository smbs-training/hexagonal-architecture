﻿using Hexagonal.Features.Role.Domain.Models;

namespace Hexagonal.Features.Role.Domain.Ports.In
{
    public interface IGetRoles
    {
        Task<List<RoleModel>> GetRoles();
    }
}
