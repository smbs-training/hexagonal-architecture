﻿namespace Hexagonal.Features.Role.Domain.Ports.In
{
    public interface IDeleteRole
    {
        Task<int> Delete(int id);
    }
}
