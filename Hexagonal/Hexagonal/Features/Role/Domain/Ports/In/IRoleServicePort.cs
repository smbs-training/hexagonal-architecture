﻿namespace Hexagonal.Features.Role.Domain.Ports.In
{
    public interface IRoleServicePort : IGetRoles, IGetRoleById, ICreateRole, IUpdateRole, IDeleteRole
    {
    }
}
