﻿using Hexagonal.Features.Role.Domain.Models;
using Hexagonal.Features.Role.Domain.Ports.In;
using Hexagonal.Features.Role.Domain.Ports.Out;

namespace Hexagonal.Features.Role.Application.UseCases
{
    public class UpdateRoleImpl : IUpdateRole
    {
        private readonly IRoleDbAdapterPort _adapter;

        public UpdateRoleImpl(IRoleDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<RoleModel> Update(int id, RoleModel role)
        {
            return _adapter.Update(id, role);
        }
    }
}
