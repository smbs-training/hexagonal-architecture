﻿using Hexagonal.Features.Role.Domain.Models;
using Hexagonal.Features.Role.Domain.Ports.In;
using Hexagonal.Features.Role.Domain.Ports.Out;

namespace Hexagonal.Features.Role.Application.UseCases
{
    public class GetRolesImpl : IGetRoles
    {
        private readonly IRoleDbAdapterPort _adapter;

        public GetRolesImpl(IRoleDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<List<RoleModel>> GetRoles()
        {
            return _adapter.FindAll();
        }
    }
}
