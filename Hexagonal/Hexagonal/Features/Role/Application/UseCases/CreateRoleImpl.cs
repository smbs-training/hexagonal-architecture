﻿using Hexagonal.Features.Role.Domain.Models;
using Hexagonal.Features.Role.Domain.Ports.In;
using Hexagonal.Features.Role.Domain.Ports.Out;

namespace Hexagonal.Features.Role.Application.UseCases
{
    public class CreateRoleImpl : ICreateRole
    {
        private readonly IRoleDbAdapterPort _adapter;

        public CreateRoleImpl(IRoleDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<RoleModel> Create(RoleModel role)
        {
            return _adapter.Create(role);
        }
    }
}
