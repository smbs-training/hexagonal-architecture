﻿using Hexagonal.Features.Role.Domain.Models;
using Hexagonal.Features.Role.Domain.Ports.In;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Hexagonal.Features.Role.Infrastructure
{
    [Route("api/Roles")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleServicePort _service;

        public RoleController(IRoleServicePort service)
        {
            _service = service;
        }

        // GET: api/<RoleController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RoleModel>>> Get()
        {
            return await _service.GetRoles();
        }

        // GET api/<RoleController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RoleModel>> Get(int id)
        {
            RoleModel roleModel = await _service.GetRoleById(id);
            if (roleModel == null)
            {
                return NotFound();
            }
            return roleModel;
        }

        // POST api/<RoleController>
        [HttpPost]
        public async Task<ActionResult<RoleModel>> Post([FromBody] RoleModel roleModel)
        {
            try
            {
                RoleModel modelSaved = await _service.Create(roleModel);
                return CreatedAtAction("Get", new { id = modelSaved.Id }, modelSaved);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PATCH api/<RoleController>/5
        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(int id, [FromBody] RoleModel roleModel)
        {
            try
            {
                await _service.Update(id, roleModel);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return NoContent();
        }

        // DELETE api/<RoleController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _service.Delete(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return NoContent();
        }
    }
}
