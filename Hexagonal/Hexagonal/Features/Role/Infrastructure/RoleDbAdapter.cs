﻿using Hexagonal.Features.Role.Domain.Models;
using Hexagonal.Features.Role.Domain.Ports.Out;
using Hexagonal.Shared.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace Hexagonal.Features.Role.Infrastructure
{
    public class RoleDbAdapter : IRoleDbAdapterPort
    {
        private readonly AppDbContext _db;

        public RoleDbAdapter(AppDbContext context)
        {
            _db = context;
        }

        public async Task<List<RoleModel>> FindAll()
        {
            List<RoleModel> roles = await _db.Roles
                .Select(x => x.ToModel())
                .ToListAsync();

            return roles;
        }

        public async Task<RoleModel> FindById(int id)
        {
            RoleEntity? roleFound = await _db.Roles
                .FirstOrDefaultAsync(i => i.Id == id);

            return roleFound == null
                ? throw new Exception($"Role with id {id} not found")
                : roleFound.ToModel();
        }

        public async Task<RoleModel> Create(RoleModel role)
        {
            try
            {
                RoleEntity entity = new RoleEntity().FromModel(role);
                _db.Roles.Add(entity);
                await _db.SaveChangesAsync();
                return entity.ToModel();
            }
            catch (Exception e)
            {
                throw new Exception("Error creating role", e);
            }
        }

        public async Task<RoleModel> Update(int id, RoleModel role)
        {
            RoleEntity? entity = await _db.Roles.FirstOrDefaultAsync(x => x.Id == id);

            if (entity == null)
            {
                throw new Exception("Role not found!");
            }

            try
            {
                entity.UpdatePropsFromModel(role);

                _db.Entry(entity).State = EntityState.Modified;
                await _db.SaveChangesAsync();

                return entity.ToModel();
            }
            catch (Exception e)
            {
                throw new Exception("Error updating role", e);
            }
        }

        public async Task<int> Delete(int id)
        {
            RoleEntity? roleEntity = await _db.Roles.FindAsync(id);

            if (roleEntity == null)
            {
                throw new Exception("Role not found!");
            }

            try
            {
                _db.Roles.Remove(roleEntity);

                await _db.SaveChangesAsync();

                return id;
            }
            catch (Exception e)
            {
                throw new Exception("Error deleting role", e);
            }
        }
    }
}
