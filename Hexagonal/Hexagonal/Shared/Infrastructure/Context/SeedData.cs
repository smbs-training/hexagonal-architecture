﻿using Hexagonal.Features.Course.Infrastructure;
using Hexagonal.Features.Role.Infrastructure;
using Hexagonal.Features.User.Infrastructure;

namespace Hexagonal.Shared.Infrastructure.Context
{
    public static class SeedData
    {
        public static void Initialize(AppDbContext context)
        {
            SeedRoles(context);
            SeedUsers(context);
            SeedCourses(context);
        }

        private static void SeedRoles(AppDbContext context)
        {
            if (!context.Roles.Any())
            {
                var roles = new List<RoleEntity>
                {
                    new RoleEntity { Name = "Admin", Description = "Administrator role" },
                    new RoleEntity { Name = "Instructor", Description = "Instructor role" },
                    new RoleEntity { Name = "Student", Description = "Student role" }
                };

                context.Roles.AddRange(roles);
                context.SaveChanges();
            }
        }

        private static void SeedUsers(AppDbContext context)
        {
            if (!context.Users.Any())
            {
                RoleEntity adminRole = context.Roles.First(x => x.Name == "Admin");
                RoleEntity instructorRole = context.Roles.First(x => x.Name == "Instructor");
                RoleEntity studentRole = context.Roles.First(x => x.Name == "Student");

                var users = new List<UserEntity>
                {
                    new UserEntity {
                        Name = "Pedro",
                        LastName = "Rodríguez",
                        Email = "pedro.rodriguez@outlook.com",
                        //Password = "password123",
                        Password = "$2a$15$pfXWBxApfwBI3icmhbfenOFUeKw3YZnz4xJMlbiON/HXViNmbgx7S",
                        Roles = new List<RoleEntity>(){
                            adminRole, instructorRole
                        }
                    },
                    new UserEntity {
                        Name = "Elena",
                        LastName = "García",
                        Email = "elena.garcia@outlook.com",
                        //Password = "password123",
                        Password = "$2a$15$pfXWBxApfwBI3icmhbfenOFUeKw3YZnz4xJMlbiON/HXViNmbgx7S",
                        Roles = new List<RoleEntity>(){
                            instructorRole
                        }
                    },
                    new UserEntity {
                        Name = "Gabriel",
                        LastName = "Fernández",
                        Email = "gabriel.fernandez@outlook.com",
                        //Password = "password123",
                        Password = "$2a$15$pfXWBxApfwBI3icmhbfenOFUeKw3YZnz4xJMlbiON/HXViNmbgx7S",
                        Roles = new List<RoleEntity>(){
                            studentRole
                        }
                    },
                    new UserEntity {
                        Name = "Juan",
                        LastName = "Martínez",
                        Email = "juan.martinez@outlook.com",
                        //Password = "password123",
                        Password = "$2a$15$pfXWBxApfwBI3icmhbfenOFUeKw3YZnz4xJMlbiON/HXViNmbgx7S",
                        Roles = new List<RoleEntity>(){
                            studentRole
                        }
                    },
                    new UserEntity {
                        Name = "Isabella",
                        LastName = "López",
                        Email = "isabella.lopez@outlook.com",
                        //Password = "password123",
                        Password = "$2a$15$pfXWBxApfwBI3icmhbfenOFUeKw3YZnz4xJMlbiON/HXViNmbgx7S",
                        Roles = new List<RoleEntity>(){
                            studentRole
                        }
                    },
                    new UserEntity {
                        Name = "Valentina",
                        LastName = "Hernández",
                        Email = "valentina.hernandez@outlook.com",
                        //Password = "password123",
                        Password = "$2a$15$pfXWBxApfwBI3icmhbfenOFUeKw3YZnz4xJMlbiON/HXViNmbgx7S",
                        Roles = new List<RoleEntity>(){
                            studentRole
                        }
                    }
                };

                context.Users.AddRange(users);
                context.SaveChanges();
            }
        }

        private static void SeedCourses(AppDbContext context)
        {
            if (!context.Courses.Any())
            {
                UserEntity instructorPedro = context.Users.First(x => x.Name == "Pedro");
                UserEntity instructorElena = context.Users.First(x => x.Name == "Elena");

                UserEntity studentGabriel = context.Users.First(x => x.Name == "Gabriel");
                UserEntity studentJuan = context.Users.First(x => x.Name == "Juan");
                UserEntity studentIsabella = context.Users.First(x => x.Name == "Isabella");
                UserEntity studentValentina = context.Users.First(x => x.Name == "Valentina");

                var courses = new List<CourseEntity>
                {
                    new CourseEntity
                    {
                        Name = "NestJS Course",
                        Description = "Learn NestJS for building scalable server-side applications.",
                        InstructorId = instructorPedro.Id,
                        Instructor = instructorPedro,
                        Students = new List<UserEntity>()
                        {
                            studentGabriel, studentJuan, studentIsabella, studentValentina
                        }
                    },
                    new CourseEntity
                    {
                        Name = "React Course",
                        Description = "Build modern web applications with React.",
                        InstructorId = instructorElena.Id,
                        Instructor = instructorElena,
                        Students = new List<UserEntity>()
                        {
                            studentGabriel, studentJuan, studentIsabella
                        }
                    }
                };

                context.Courses.AddRange(courses);
                context.SaveChanges();
            }
        }
    }
}
