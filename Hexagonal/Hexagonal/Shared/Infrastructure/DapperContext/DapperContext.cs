﻿using Hexagonal.Shared.Infrastructure.Context;
using Microsoft.Data.SqlClient;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq.Expressions;
using System.Reflection;

namespace Hexagonal.Shared.Infrastructure.DapperContext
{
    public class DapperContext
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;

        public DapperContext(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DatabaseConnection") ?? string.Empty;
        }

        public IDbConnection CreateConnection()
            => new SqlConnection(_connectionString);

    }
}
